import { Icon } from '@iconify/react';
import calendarFill from '@iconify/icons-eva/calendar-outline';
// material
import { alpha, experimentalStyled as styled } from '@material-ui/core/styles';
import { Box, Card, Typography, Stack } from '@material-ui/core';
// utils
import { fDate } from 'src/utils/formatTime';

// ----------------------------------------------------------------------

const IconWrapperStyle = styled('div')(({ theme }) => ({
  width: 24,
  height: 24,
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.success.main,
  backgroundColor: alpha(theme.palette.success.main, 0.16)
}));

// ----------------------------------------------------------------------



export default function RepaymentDate() {


  return (
    <Card sx={{ display: 'flex', alignItems: 'center', p: 3 }}>
      <Box sx={{ flexGrow: 1 }}>
        <Stack direction="row" alignItems="center" spacing={1} sx={{ mt: 2, mb: 1 }}>
          <IconWrapperStyle>
            <Icon width={16} height={16} icon={calendarFill} />
          </IconWrapperStyle>
          <Typography component="span" variant="subtitle2">
            Зээл төлөх өдөр
          </Typography>
        </Stack>

        <Typography variant="h3">{fDate(new Date())}</Typography>
      </Box>
    </Card>
  );
}
