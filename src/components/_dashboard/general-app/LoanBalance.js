import { Icon } from '@iconify/react';
import trendingDownFill from '@iconify/icons-eva/trending-down-fill';
// material
import { alpha, useTheme, experimentalStyled as styled } from '@material-ui/core/styles';
import { Box, Card, Typography, Stack } from '@material-ui/core';
// utils
import { fNumber } from '../../../utils/formatNumber';

// ----------------------------------------------------------------------

const IconWrapperStyle = styled('div')(({ theme }) => ({
  width: 24,
  height: 24,
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.success.main,
  backgroundColor: alpha(theme.palette.success.main, 0.16)
}));

// ----------------------------------------------------------------------

const BALANCE = 4876;

export default function LoanBalance() {
  const theme = useTheme();


  return (
    <Card sx={{ display: 'flex', alignItems: 'center', p: 3 }}>
      <Box sx={{ flexGrow: 1 }}>
        <Stack direction="row" alignItems="center" spacing={1} sx={{ mt: 2, mb: 1 }}>
          <IconWrapperStyle
            sx={{
              color: 'error.main',
              bgcolor: alpha(theme.palette.error.main, 0.16)
            }}
          >
            <Icon width={16} height={16} icon={trendingDownFill} />
          </IconWrapperStyle>
          <Typography component="span" variant="subtitle2">
            Зээлийн үлдэгдэл
          </Typography>
        </Stack>

        <Typography variant="h3">{fNumber(BALANCE)} MNT</Typography>
      </Box>
    </Card>
  );
}
