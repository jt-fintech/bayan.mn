import { Icon } from '@iconify/react';
import trendingUpFill from '@iconify/icons-eva/trending-up-fill';
// material
import { alpha, experimentalStyled as styled } from '@material-ui/core/styles';
import { Box, Card, Typography, Stack } from '@material-ui/core';
// utils
import { fNumber } from '../../../utils/formatNumber';

// ----------------------------------------------------------------------

const IconWrapperStyle = styled('div')(({ theme }) => ({
  width: 24,
  height: 24,
  display: 'flex',
  borderRadius: '50%',
  alignItems: 'center',
  justifyContent: 'center',
  color: theme.palette.success.main,
  backgroundColor: alpha(theme.palette.success.main, 0.16)
}));

// ----------------------------------------------------------------------

const AMOUNT = 100000;

export default function AvailableLoanAmount() {

  return (
    <Card sx={{ display: 'flex', alignItems: 'center', p: 3 }}>
      <Box sx={{ flexGrow: 1 }}>
        {/* <Typography variant="subtitle2"></Typography> */}
        <Stack direction="row" alignItems="center" spacing={1} sx={{ mt: 2, mb: 1 }}>
          <IconWrapperStyle>
            <Icon width={16} height={16} icon={trendingUpFill} />
          </IconWrapperStyle>
          <Typography component="span" variant="subtitle2">
            Боломжит зээлийн хэмжээ
          </Typography>
        </Stack>

        <Typography variant="h3">{fNumber(AMOUNT)} MNT</Typography>
      </Box>
    </Card>
  );
}
