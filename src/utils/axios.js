import axios from 'axios';

// ----------------------------------------------------------------------

const axiosInstance = axios.create({
  baseURL: process.env.NODE_ENV === "production" ? "/ons/api" : "/ons"
});

axiosInstance.interceptors.response.use(
  (response) => response,
  (error) => { console.log(error); return Promise.reject((error.response && error.response.data) || 'Something went wrong') }
);

export default axiosInstance;
