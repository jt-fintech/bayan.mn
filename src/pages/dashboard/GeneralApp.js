// material
import { Container, Grid } from '@material-ui/core';
// hooks
import useAuth from '../../hooks/useAuth';
// components
import Page from '../../components/Page';
import {
  AppWelcome,
  AppFeatured,
  RepaymentDate,
  LoanBalance,
  AvailableLoanAmount,
} from '../../components/_dashboard/general-app';
import { useEffect } from 'react';
import api from 'src/utils/axios'
const GeneralApp = () => {
  const { user } = useAuth();
  useEffect(() => {
    const fetchData = async () => {
      await api.post("/account/loan/search", []);
    }

    fetchData();
  }, []);
  return (
    <Page title="Эхлэл">
      <Container maxWidth="xl">
        <Grid container spacing={3}>
          <Grid item xs={12} md={8}>
            <AppWelcome displayName={user.firstName} />
          </Grid>

          <Grid item xs={12} md={4}>
            <AppFeatured />
          </Grid>

          <Grid item xs={12} md={4}>
            <AvailableLoanAmount />
          </Grid>

          <Grid item xs={12} md={4}>
            <LoanBalance />
          </Grid>

          <Grid item xs={12} md={4}>
            <RepaymentDate />
          </Grid>

          {/* <Grid item xs={12} md={6} lg={4}>
            <AppCurrentDownload />
          </Grid>

          <Grid item xs={12} md={6} lg={8}>
            <AppAreaInstalled />
          </Grid>

          <Grid item xs={12} lg={8}>
            <AppNewInvoice />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopRelated />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopInstalledCountries />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <AppTopAuthors />
          </Grid>

          <Grid item xs={12} md={6} lg={4}>
            <Grid container spacing={3}>
              <Grid item xs={12}>
                <AppWidgets1 />
              </Grid>
              <Grid item xs={12}>
                <AppWidgets2 />
              </Grid>
            </Grid>
          </Grid> */}
        </Grid>
      </Container>
    </Page>
  );
}
export default GeneralApp;